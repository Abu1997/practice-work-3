import { Component } from '@angular/core';
import { EnrollmentService } from './enrollment.service';
import { User } from './user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  topics = ['Angular','React','Vue'];
  errorMsg = '';

  userModel = new User('','rob@tm',2535256898,'default','morning',true);

  constructor(private _enrollmentService: EnrollmentService){}

  onSubmit() {
    this._enrollmentService.enroll(this.userModel)
    .subscribe(
      data => console.log('Success!',data),
      error => this.errorMsg = error.statusText
    )
  }
  
}

